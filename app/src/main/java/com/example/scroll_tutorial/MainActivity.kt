package com.example.scroll_tutorial

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import java.io.Serializable

@kotlinx.serialization.Serializable
data class Product(
    var id:String,
    var name:String,
    var image_url:String,
    var description:String,
    var price:Int
) : Serializable


const val EXTRA_DESCRIPTION = "com.example.scroll_tutorial.DESCRIPTION"
const val PRODUCTS_HTTP = "https://itimkeno-app.herokuapp.com/products"

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO:
        // change LinearLayout  to  ConstraintLayout
        // edit UI completely
        val scrollView = ScrollView(this)
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        scrollView.layoutParams = layoutParams

        val linearLayout = LinearLayout(this)
        val linearParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.layoutParams = linearParams

        scrollView.addView(linearLayout)

        // TODO: change deprecated methods
        val productsArray: ArrayList<Product> =
            GetJsonDataRemote().execute(PRODUCTS_HTTP).get()!!

        println(productsArray.size)

        // TODO: implement single class for loading products
        for (product in productsArray) {
            val imageView = ImageView(this)
            val paramsImage = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            paramsImage.setMargins(0, 30, 0, 30)
            paramsImage.gravity = Gravity.CENTER
            imageView.layoutParams = paramsImage

            val filePath = product.image_url // this is image file ( url ) path

            // TODO: change deprecated methods
            GetImageDataRemote(
                this,
                imageView
            ).execute(filePath)
            linearLayout.addView(imageView)

            val textView = TextView(this)
            val paramsText = LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            paramsText.setMargins(0, 0, 0, 30)
            paramsText.gravity = Gravity.CENTER
            textView.layoutParams = paramsText
            textView.scaleX = 2F
            textView.scaleY = 2F
            textView.text = "price: ${product.price}"
            linearLayout.addView(textView)

            val context = this
            imageView.setOnClickListener {
                openDescriptionScreen(context, product)
            }
        }

        val linearLayout1 = findViewById<LinearLayout>(R.id.rootContainer)
        linearLayout1?.addView(scrollView)

    }

    private fun openDescriptionScreen(context: Context?, product :Product?): Intent
    {
        val intent = Intent(context, DescriptionPageActivity::class.java)
        intent.putExtra(EXTRA_DESCRIPTION, product)
        startActivity(intent)
        return intent
    }
}