package com.example.scroll_tutorial

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import org.json.JSONArray
import org.json.JSONTokener
import java.net.URL

@SuppressLint("StaticFieldLeak")
@Suppress("DEPRECATION")
class GetImageDataRemote(context: Context, var imageView: ImageView) : AsyncTask<String, Void, Bitmap?>() {
    init {
       Toast.makeText(context, "Please wait, loading image...",     Toast.LENGTH_SHORT).show()
    }
    override fun doInBackground(vararg url: String): Bitmap? {
        val imageURL = url[0]
        var image: Bitmap? = null
        try {
            val `is` = URL(imageURL).openStream()
            BitmapFactory.decodeStream(`is`).also { image = it }
        }
        catch (e: Exception) {
            Log.e("Error Message", e.message.toString())
            e.printStackTrace()
        }
        return image
    }
    override fun onPostExecute(result: Bitmap?) {
        imageView.setImageBitmap(result)
    }
}
//
@SuppressLint("StaticFieldLeak")
@Suppress("DEPRECATION")
class GetJsonDataRemote : AsyncTask<String, Void, ArrayList<Product>?>() {

    private var productsArray = ArrayList<Product>()

    override fun doInBackground(vararg urls: String): ArrayList<Product> {

       val adr = urls[0]
       val apiResponse = URL(adr).readText()

       val jsonArray = JSONTokener(apiResponse).nextValue() as JSONArray?
       if (jsonArray != null) {

           for (i in 0 until jsonArray.length()) {

               val id = jsonArray.getJSONObject(i).getString("_id")
               Log.i("ID: ", id)

               val prodName = jsonArray.getJSONObject(i).getString("name")
               Log.i("name: ", prodName)

               val prodImg = jsonArray.getJSONObject(i).getString("image_url")
               Log.i("image_url: ", prodImg)

               val prodDescr = jsonArray.getJSONObject(i).getString("description")
               Log.i("description: ", prodDescr)

               val prodPrice = jsonArray.getJSONObject(i).getInt("price")
               Log.i("price: ", prodPrice.toString())

               productsArray.add(Product(id, prodName, prodImg, prodDescr, prodPrice))
           }
       }

       return productsArray
    }

    override fun onPostExecute(result: ArrayList<Product>?) {

        if (result != null) {
            println("onPostExecute success")
        }
        else {
            println("onPostExecute can't load products")
        }
    }
}
