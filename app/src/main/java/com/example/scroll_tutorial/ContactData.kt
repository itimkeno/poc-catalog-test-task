package com.example.scroll_tutorial

import android.os.Bundle
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout


class ContactDataActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.contact_data)

        val layout = ConstraintLayout(this)
        val layoutParams = ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layout.layoutParams = layoutParams

        val button = findViewById<Button>(R.id.button)
        button.setOnClickListener {
            println("button click, TODO: implement transfer and save input data")
        }


        val thatLayout = findViewById<ConstraintLayout>(R.id.contactLayout)
        thatLayout?.addView(layout)
    }
}
