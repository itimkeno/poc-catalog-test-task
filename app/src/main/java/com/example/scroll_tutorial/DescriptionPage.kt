package com.example.scroll_tutorial

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity


class DescriptionPageActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description_page)

        val product = intent.extras?.get(EXTRA_DESCRIPTION) as? Product

        val linearLayout = LinearLayout(this)
        val linearParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.layoutParams = linearParams

        val nameView = TextView(this)
        val paramsNameText = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        paramsNameText.setMargins( 0, 20, 0, 40)
        paramsNameText.gravity = Gravity.CENTER
        nameView.layoutParams = paramsNameText
        nameView.scaleX = 3F
        nameView.scaleY = 3F
        nameView.text = "${product?.name}"
        linearLayout.addView(nameView)

        val imageView = ImageView(this)
        val paramsImage = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        paramsImage.setMargins(0, 30, 0, 30)
        paramsImage.gravity = Gravity.CENTER
        imageView.layoutParams = paramsImage

        val filePath = product?.image_url
        GetImageDataRemote(
            this,
            imageView
        ).execute(filePath)
        linearLayout.addView(imageView)

        val priceView = TextView(this)
        val paramsPriceText = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        paramsPriceText.setMargins( 0, 10, 0, 30)
        paramsPriceText.gravity = Gravity.CENTER
        priceView.layoutParams = paramsPriceText
        priceView.scaleX = 2F
        priceView.scaleY = 2F
        priceView.text = "price: ${product?.price}"
        linearLayout.addView(priceView)

        val descriptionView = TextView(this)
        val paramsDescriptionText = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        paramsDescriptionText.setMargins(0, 10, 0, 10)
        paramsDescriptionText.gravity = Gravity.START
        descriptionView.layoutParams = paramsDescriptionText
        descriptionView.text = "Product Description\n    ${product?.description}"
        linearLayout.addView(descriptionView)

        val button = Button(this)
        val paramsButton = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        paramsButton.setMargins(50, 100, 50, 100)
        paramsButton.gravity = Gravity.CENTER
        button.text = "Buy now"
        linearLayout.addView(button)

        button.setOnClickListener {
            println("button click")
            openContactEdit()
        }

        val linearLayout1 = findViewById<LinearLayout>(R.id.descriptionLayout)
        linearLayout1?.addView(linearLayout)
    }

    private fun openContactEdit(): Intent
    {
        val intent = Intent(this, ContactDataActivity::class.java)
        startActivity(intent)
        return intent
    }
}